# Changelog

## Unreleased

### Fixed

- Mise à jour documentation d’upgrade
- Mise à jour des dépendances

## 5.0.0-beta - 2024-12-03

### Changed

- Les dossiers `ecrire`, `prive`, `squelettes-dist`, `plugins-dist` s'installent avec Composer
- L’écran de sécurité s’installe avec Composer
- Le Changelog concernant `ecrire` et `prive` est déplacé dans leurs dépôts respectifs.